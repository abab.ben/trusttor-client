import React from 'react';
import { Route,Switch } from "react-router-dom";
import Animation from './Components/Animation';
import Grid from './Components/Grid';
import Search from './Components/Search';
// import './App.css';

function Routes() {
    return (
        <Switch>
            <Route exact path="/" component={Grid} />
            <Route path="/search" component={Search} />
            <Route path="/animation" component={Animation} />
        </Switch>
    );
}

export default Routes;
