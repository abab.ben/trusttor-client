import React, {PropTypes} from 'react';
import { Comment } from 'antd';

class CommentWrapper extends React.Component {
    
    render() {
        const {comment} = this.props;
        const {name,email,body} = comment;
        return (
            <Comment
                datetime={email}
                author={<a>{name}</a>}
                content={<p>{body}</p>}
            />
        );
    }
}

CommentWrapper.propTypes = {};

export default CommentWrapper;