import React from "react";
import "antd/dist/antd.css";
import { List, message, Spin,Card } from "antd";

import InfiniteScroll from "react-infinite-scroller";

import {fetchData} from '../apicalls';

const imagesCount = 5000;
const photosUrl = "photos";

class Grid extends React.Component {
    state = {
        data: [],
        loading: false,
        hasMore: true
    };
    
    componentDidMount() {
        fetchData(photosUrl,res => {
            this.setState({
                data: res
            });
        });
    }
    
    handleInfiniteOnLoad = () => {
        let { data } = this.state;
        if(!data || data.length === 0) return;
        this.setState({
            loading: true
        });
        if (data.length > imagesCount) {
            this.setState({
                hasMore: false,
                loading: false
            });
            return;
        }
        
        fetchData(photosUrl, res => {
            this.setState({
                data:data.concat(res),
                loading: false
            });
        });
    };
    
    render() {
        const {data,loading,hasMore} = this.state;

        return (
            <div className="demo-infinite-container">
                <InfiniteScroll
                    initialLoad={true}
                    pageStart={0}
                    loadMore={this.handleInfiniteOnLoad}
                    hasMore={!loading && hasMore}
                >
                    <List
                        grid={{ column: "5" }}
                        dataSource={data || []}
                        renderItem={item => (
                            <List.Item key={item.id}>
                                <Card
                                    style={{ width: 240 }}
                                    cover={<img alt="example" src={item.thumbnailUrl} />}
                                >
                                </Card>
                            </List.Item>
                        )}
                    >
                        {loading && hasMore && (
                            <div className="demo-loading-container">
                                <Spin />
                            </div>
                        )}
                    </List>
                </InfiniteScroll>
            </div>
        );
    }
}

export default Grid;