import React, {PropTypes} from 'react';
import { Layout, Button, Breadcrumb, Icon ,List, Card,Row,Col } from 'antd';
import './animation.css'
class Animation extends React.PureComponent {
    
    
    render() {
        return (
        <Layout>
            <Row>
                <Col offset={6}  span={4}>    <Button className="grow" type="primary">grow</Button></Col>
                <Col span={4}>    <Button className="shrink" type="primary">shrink</Button></Col>
                <Col span={4}>    <Button className="color" type="primary">color</Button></Col>
            </Row>
            <br/>
            <Row>
                <Col span={4} offset={10}>    <Button className="rotate" type="primary">rotate</Button></Col>
            </Row>
        </Layout>
        );
    }
}

Animation.propTypes = {};

export default Animation;

