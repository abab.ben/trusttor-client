import React, {PropTypes} from 'react';
import { Layout, AutoComplete } from 'antd';
import 'antd/dist/antd.css';
import CommentWrapper from '../Components/Comment';
import {fetchData} from '../apicalls';

const apiDomainEmails = "users";
const apiDomainComments = "comments";

class Search extends React.Component {
    
    state = {
        value: '',
        comments: [],
        emails: [],
    };

    componentDidMount(){
        fetchData(apiDomainEmails,response => {
            if(response){
                this.setState({
                    isLoaded: true,
                    emails: response
                });
            }
        });
    }
    
     onSelect = value => {
         fetchData(`${apiDomainComments}?user=${encodeURI(value)}`,response => {
             if(response){
                 this.setState({
                     isLoaded: true,
                     comments: Array.isArray(response) ? response :[response]
                 });
             }
             this.setState({ comment: value.body });
         });
    };
    
    filterOption = (inputValue,option) => {
        return option.key.includes(inputValue)
    };
    
    render() {
        const { emails,comments } = this.state;
        return (
            <Layout>
                    <AutoComplete
                        dataSource={emails}
                        style={{ width: 200 }}
                        onSelect={this.onSelect}
                        filterOption={this.filterOption}
                        defaultActiveFirstOption={false}
                        //onSearch={this.onSearch}
                        placeholder="search by email"
                    />
                    <br />
                    {comments.map(comment=>
                        <CommentWrapper comment={comment} key={comment.name}></CommentWrapper>
                    )}
                
            </Layout>
        );
    }
}

Search.propTypes = {};

export default Search;