import React, {PropTypes} from 'react';
import { Comment } from 'antd';
import { Card } from 'antd';

const { Meta } = Card;

class CardWrapper extends React.Component {
    
    render() {
        const {thumb} = this.props;
        return (
            <Card
                hoverable
                style={{ width: 240 }}
                cover={<img alt="example" src={thumb} />}
            >
            
            </Card>
        );
    }
}

CardWrapper.propTypes = {};

export default CardWrapper;
