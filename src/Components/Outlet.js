import React from 'react';
import { Layout, Menu, Icon } from 'antd';
import { Link } from "react-router-dom";

import 'antd/dist/antd.css';
import Routes from '../router';
const { Content, Sider } = Layout;

class Outlet extends React.Component {
    
    render() {
        return (
            <Layout>
                <Layout>
                    <Sider   style={{
                        overflow: 'auto',
                        height: '100vh',
                        position: 'fixed',
                        left: 0,
                    }}>
                        <div className="logo" />
                        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
                            <Menu.Item key="1">
                                <Icon type="pie-chart" />
                                <span>Grid</span>
                                <Link to="/" />
                            </Menu.Item>
                            <Menu.Item key="2">
                                <Icon type="desktop" />
                                <span>Search</span>
                                <Link to="/search" />
                            </Menu.Item>
                            <Menu.Item key="9">
                                <Icon type="file" />
                                <span>Animation</span>
                                <Link to="/animation" />
                            </Menu.Item>
                        </Menu>
                    </Sider>
                    <Layout style={{ minHeight: '100vh', marginLeft: 200 }}>
                        <Content style={{ margin: '0 16px' }}>
                            <div style={{ padding: 24, minHeight: 360 }}>
                                <Routes/>
                            </div>
                        </Content>
                    </Layout>
                </Layout>
            </Layout>
        
        );
    }
}

export default  Outlet