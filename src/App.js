import React from 'react';
import Outlet from '../src/Components/Outlet'
import { BrowserRouter as Router, Route } from "react-router-dom";
import logo from './logo.svg';
import Animation from './Components/Animation';
import Grid from './Components/Grid';
import Search from './Components/Search';
// import './App.css';

function App() {
  return (
      <Router>
          <Outlet/>
      </Router>
  );
}

export default App;
