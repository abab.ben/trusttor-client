import reqwest from 'reqwest';
const apiDomain = "https://localhost:44309/api/";


//const apiDomain = "https://jsonplaceholder.typicode.com/";


export default (url)=>{
    return fetch(`${apiDomain}${url}`)
    .then(res => res.json())
    .then(
       
        (result) => {
            return result;
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
            return false;
        }
    );
}

export const fetchData = (url,callback) => {
        reqwest({
            url: `${apiDomain}${url}`,
            type: "json",
            method: "get",
            contentType: "application/json",
            success: res => {
                callback(res);
            }
        });
    };
